import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item {
    id:mainWindow

    FileDialog
    {
        id: soundOpen
        title: ""
        onAccepted: {

            soundOpen.close();
        }
        onRejected: {
            soundOpen.close();
        }
    }

    Popup {
            id: filterPopup
            x: filterButton.x
            y: filterButton.y + filterButton.height + 10
            width: 200
            height: 300
            focus: true
            closePolicy: Popup.OnPressOutside


        }

    Popup {
            id: sortPopup
            x: sortButton.x
            y: sortButton.y + filterButton.height + 10
            width: 100
            height: 150
            focus: true
            closePolicy: Popup.OnPressOutside

            ColumnLayout{

                id:sortParms
                anchors.top: sortPopup.top
                anchors.bottom: sortPopup.bottom

                 RadioButton{
                     id:titleParm
                     text: "Title"
                     scale: 0.6
                 }
                 RadioButton{
                     id:durationParm
                     text:"Duration"
                     scale: 0.6
                 }
                 RadioButton{
                     id:sizeParm
                     text:"Size"
                     scale: 0.6
                 }

                 RadioButton{
                     id:bitrateParm
                     text:"Bitrate"
                     scale: 0.6
                 }
                 RadioButton{
                     id:yearParm
                     text:"Year"
                     scale: 0.6
                 }
             }

        }

    ScrollView {
        anchors.top : parent.top
        anchors.left : parent.left
        anchors.right : parent.right
        anchors.bottom: progresSlider.top

        ListView {
            id: listView
            anchors.centerIn: parent
            implicitWidth: parent.width
            height: parent.height / 3 * 2
            model: 20
            spacing: 3
            delegate: ItemDelegate {
                id: itemDelegate

                Item {
                    id: rect1
                    width: itemDelegate.width / 5
                    height: itemDelegate.height / 2
                    anchors {
                        left: itemDelegate.left
                        top: itemDelegate.top
                    }
                    Text {
                        anchors {
                            verticalCenter: rect1.verticalCenter
                            horizontalCenter: rect1.horizontalCenter
                        }
                        id: name1
                        font.bold: true
                        text: qsTr("Music name")
                    }

                }

                Item {
                    id: rect2
                    width: itemDelegate.width / 5
                    height: itemDelegate.height / 2
                    anchors {
                        left: itemDelegate.left
                        top: rect1.bottom
                    }

                    Text {
                        anchors {
                            verticalCenter: rect2.verticalCenter
                            horizontalCenter: rect2.horizontalCenter
                        }
                        id: name2
                        text: qsTr("Singer")
                    }
                }

                Item {
                    id: rect3
                    width: itemDelegate.width / 5
                    height: itemDelegate.height
                    anchors {
                        right: itemDelegate.right
                        rightMargin: 50
                        top: itemDelegate.top
                    }
                    Text {
                        anchors {
                            verticalCenter: rect3.verticalCenter
                            horizontalCenter: rect3.horizontalCenter
                        }
                        font.bold: true
                        id: name3
                        text: qsTr("5:02")
                    }
                }

                Item {
                    id: rect4
                    width: 20
                    height: 20
                    anchors {
                        left: rect3.right
                        verticalCenter: itemDelegate.verticalCenter
                    }
                    Rectangle {
                        width: rect4.width
                        height: rect4.height
                        border.color: "#303444"
                        border.width: 2
                        radius: (width)

                    }
                    Text {
                        anchors {
                            verticalCenter: parent.verticalCenter
                            horizontalCenter: parent.horizontalCenter
                        }
                        font.pixelSize: 15
                        font.bold: true
                        id: name4
                        color: "#303444"
                        text: qsTr("i")
                    }
                }
                width: listView.width
            }
        }
    }

    Rectangle{
        id:tbBackground // top block background
        height :parent.height/10
        color: "#20b2aa"

        anchors.left: parent.left
        anchors.right : parent.right

        RowLayout
        {
            id:topButtons
            anchors.left: tbBackground.left
            anchors.top : tbBackground.top
            anchors.bottom : tbBackground.bottom
            anchors.leftMargin: 3

            Button{
                id:sortButton
                text: "Sort by..."
                background: Rectangle
                {
                    radius : 3
                    color : sortButton.down? "grey" : "white"
                }
                onClicked: sortPopup.open();
            }


            Button{
                id:filterButton
                text: "Filter"
                background: Rectangle
                {
                    width: filterButton.width
                    radius:3

                    color: filterButton.down? "grey" : "white"
                }

                onClicked: filterPopup.open();
            }

        }


        RowLayout{

            id:searchBlock
            anchors.right: tbBackground.right
            anchors.top : tbBackground.top
            anchors.bottom : tbBackground.bottom


            Button{
                id:searchButton
                text: "Search"

                enabled: (searchField.text == "")? false : true
                background: Rectangle
                {
                    width: searchButton.width
                    radius:3
                    color: searchButton.down? "grey" : "white"
                }
            }

            TextField{
                id:searchField
                placeholderText: "Enter song title..."

                height: tbBackground*0.8

                Layout.alignment: Qt.AlignHCenter
                //anchors.top : tbBackground.top
                //anchors.bottom : tbBackground.bottom

            }
        }

    }

    Rectangle
    {
        id:cbBackground

        height: parent.height/5
        width: parent.width

        anchors.bottom: parent.bottom
        color: "#20b2aa"

        RowLayout
        {
            id:controlBar
            height: 30

            anchors.centerIn: cbBackground
            anchors.top: cbBackground.top
            anchors.bottom: cbBackground.bottom


            RoundButton{
                id: stopButton
                icon.source: "/png/003-stop.png"

            }

            RoundButton{
                id: previousButton
                icon.source: "/png/011-back.png"
            }

            RoundButton{
                id: playButton
                width:height
                icon.source: "/png/013-play.png"
            }

            RoundButton{
                id: nextButton
                icon.source: "/png/001-next.png"
                Layout.alignment: Qt.AlignLeft
            }

            RoundButton{
                id: openButton
                icon.source: "/png/028-folder.png"
                Layout.alignment: Qt.AlignLeft
                onPressed: {
                    soundOpen.open();
                }
            }

        }


        Slider{
            id:volumeSlider
            from:0
            to:100
            stepSize: 1
            scale: 0.5

            anchors.top : cbBackground.top
            anchors.bottom: cbBackground.bottom
            anchors.right:cbBackground.right
            anchors.margins: 10

        }

    }


    RowLayout{
        id:progresSlider
        anchors.bottom: cbBackground.top
        anchors.right:  cbBackground.right
        anchors.left: cbBackground.left


        Slider{
            id:durationSlider
            implicitWidth: parent.width - 40
            from:0
            to:100
            stepSize: 1

            handle: Rectangle {
                  }

            onMoved: {
                durationText.text = durationSlider.value
            }
        }
        Text{
            id:durationText
            Layout.alignment: Qt.AlignCenter
            text:"00:0ew0"
        }



    }

}

