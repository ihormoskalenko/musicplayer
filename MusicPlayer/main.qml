import QtQuick 2.12
import QtQuick.Window 2.12

Window {
    width: 640
    height: 480
    minimumHeight: 480
    minimumWidth: 640

    visible: true
    title: qsTr("Hello World")

    MainForm{
        id:mainForm
        anchors.fill : parent
    }
}

